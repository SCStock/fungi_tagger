# Fungi Tagger

A tool to facilitate the determination of root mycorrhization.

![alt text](FungiTaggerScreenshot.png "Screenshot FungiTagger")

## Introduction

Counting grid tiles for determining the degree of root mycorrhization can be exhausting and frustrating when loosing track of the current grid position or shifting in row or column. The Fungi Tagger allows to easily count total root segments and infected segments, as it keeps track of the current position and the assigned characteristics (fungi, root, no root), while passing along each grid tile. That way shifting in column or row within the grid is not an issue anymore and it is possible to interupt and continue later on.

## Installation

- Install Python 3.
- Install dependencies by running `pip install -r requirements.txt`
- Run `python FungiTagger.py`
- Open `Example.jpg` from folder `example` to test

## Usage

- Start the Fungi Tagger and load an image file (.png, .jpg, .jpeg, .tiff, .tif)
- An input dialog will open, allowing you to insert the desired grid scale (in pixel)
- Use the arrow keys (left, down, right) to assign a characterisic = tag (fungi, root, no root) to each grid tile
	Note: black tiles will be scipped automatically
- Use the delete key to go back and delete the previous assignments 
- The tags will be saved in an output file when closing the program
- When opening an image, the Fungi Tagger will check if an output file already exists. If yes, it will check for the last position saved in the file and jumps to the following one. No scale input dialog will open.
