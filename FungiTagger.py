#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    A tool for counting root segemnts and hyphae
"""
# Svenja C. Stock - svenja.stock@uni-tuebingen.de
import sys
import os
import numpy as np
import pandas
import PIL as pil
import pyqtgraph as pg
from PIL import Image
from pyqtgraph import QtCore, QtGui
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtWidgets import QApplication, QFileDialog, QMessageBox, QInputDialog
from PyQt5.QtCore import Qt

__software__ = "Fungi Tagger"
__version__ = "v1.0"

class FungiTaggerMainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        Image.MAX_IMAGE_PIXELS = None
        
        print(__software__)
        print('Version: ' + __version__)
        self.setWindowTitle(__software__ + ' ' + __version__)

        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), Qt.black)
        self.setPalette(p)
        self.isFileLoaded = False

        self.fungiTaggerWidget = FungiTaggerWidget(parent=self)
        self.setCentralWidget(self.fungiTaggerWidget)

        bar = self.menuBar()
        bar.setStyleSheet("""
        QMenuBar {
            background-color: rgb(49,49,49);
            color: rgb(255,255,255);
            border: 1px solid ;
        }
        QMenuBar::item {
            background-color: rgb(49,49,49);
            color: rgb(255,255,255);
        }
        QMenuBar::item::selected {
            background-color: rgb(30,30,30);
        }
        """)
        fileMenu = bar.addMenu('File')
        fileMenu.setStyleSheet("""
            QMenu {
                background-color: rgb(49,49,49);
                color: rgb(255,255,255);
                border: 1px solid ;
            }
            QMenu::item::selected {
                background-color: rgb(30,30,30);

            }
        """)
        openFileAction = QtWidgets.QAction('Open root image', self)
        closeAction = QtWidgets.QAction('Close', self)
        fileMenu.addAction(openFileAction)
        fileMenu.addAction(closeAction)
        
        openFileAction.triggered.connect(self.openFileDialog)
        closeAction.triggered.connect(self.close)

    def openFileDialog(self):
        self.resetView()
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"Choose a root file", "","Only nice root images (*.png *.jpg *.jpeg *.tiff *.tif)", options=options)
        if fileName:
            self.fungiTaggerWidget.loadImage(fileName)
            if not self.fungiTaggerWidget.isTagFileAvailable():
                self.openScaleInputDialog()
            else:
                self.fungiTaggerWidget.setFirstRoi(0)

    def openScaleInputDialog(self):
        i, okPressed = QInputDialog.getInt(self, "Enter scale size","Scale size (px):", self.fungiTaggerWidget.scaleSize, 0, 300, 1)
        if okPressed:
            self.fungiTaggerWidget.setFirstRoi(i)

    def showSummary(self, imageName, tagNoRoot, tagBlack, tagNoFungi, tagFungi):
        messageBoxText = 'No root: {}\nNo Fungi: {}\nFungi: {}'.format(tagNoRoot+tagBlack, tagNoFungi, tagFungi)
        messageBoxTitle = '-{}-'.format(imageName) 
        buttonClose = QMessageBox.information(self, messageBoxTitle, messageBoxText, QMessageBox.Close)
        if buttonClose == QMessageBox.Close:
            self.close()

    def resetView(self):
        if not (self.fungiTaggerWidget.imgFull.image is None):
            self.saveResults()
            del self.fungiTaggerWidget
            self.fungiTaggerWidget = FungiTaggerWidget(parent=self)
            self.setCentralWidget(self.fungiTaggerWidget)
            
    def closeEvent(self, event):
        self.fungiTaggerWidget.saveResults()
        self.close()

class FungiTaggerWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent

        # create and set layout to place widgets
        grid_layout = QtWidgets.QGridLayout(self)

        # set parameter
        self.displayFilename = None
        self.scaleSize = 181
        self.lstFungiTag = []
        pg.setConfigOptions(imageAxisOrder='row-major')
        pg.setConfigOptions(antialias=True)
        self.pgView = pg.GraphicsLayoutWidget(border=(100,100,100))

        # FullImageView
        self.fullImageWindow = self.pgView.addLayout(row=0, col=0)
        self.imageNameText = ''
        self.imageNameLabel = self.fullImageWindow.addLabel(self.imageNameText, row=0, col=0, bold=True, size='12pt')
        self.fullImageViewBox = self.fullImageWindow.addViewBox(row=1, col=0, lockAspect=True, enableMenu=False)
        self.fullImageViewBox.disableAutoRange('xy')
        self.fullImageViewBox.autoRange()
        self.fullImageViewBox.invertY()
        self.imgFull = pg.ImageItem()
        self.fullImageViewBox.addItem(self.imgFull)
        self.descriptionText = 'Use the mouse to move the image content (click and hold) and zoom in or out (mousewheel)'
        self.descriptionLabel = self.fullImageWindow.addLabel(self.descriptionText, row=2, col=0)

        # ZoomImageView
        self.zoomImageWindow = self.pgView.addLayout(row=1, col=0)
        self.zoomImageLabelText = 'Find the fungi!'
        self.zoomImageLabel = self.zoomImageWindow.addLabel(self.zoomImageLabelText, row=0, col=0)
        self.zoomImageViewBox = self.zoomImageWindow.addViewBox(row=1, col=0, lockAspect=True, enableMenu=False)
        self.zoomImageViewBoxGrid = pg.GridItem()
        self.zoomImageViewBox.addItem(self.zoomImageViewBoxGrid)
        self.zoomImageViewBox.disableAutoRange('xy')
        self.zoomImageViewBox.autoRange()
        self.zoomImageViewBox.invertY()
        self.imgZoom = pg.ImageItem()
        self.zoomImageViewBox.addItem(self.imgZoom)

        # ButtonBar
        self.buttonNoFungi = QtWidgets.QPushButton('Root - No Fungi\n(Press arrow down)')
        self.buttonNoFungi.clicked.connect(self.tagNoFungi)
        self.buttonNoRoot = QtWidgets.QPushButton('No Root\n(Press arrow right)')
        self.buttonNoRoot.clicked.connect(self.tagNoRoot)
        self.buttonFungi = QtWidgets.QPushButton('Fungi ! Woopwoop ! \n(Press arrow left)')
        self.buttonFungi.clicked.connect(self.tagFungi)

        grid_layout.addWidget(self.pgView, 0, 0, 1, 3)
        grid_layout.addWidget(self.buttonNoFungi, 1, 1)
        grid_layout.addWidget(self.buttonNoRoot, 1, 2)
        grid_layout.addWidget(self.buttonFungi, 1, 0)

    def tagNoFungi(self):
        if (self.parent.isFileLoaded):
            self.tagRoi('ROOT')
            self.stepToNextRoi()

    def tagNoRoot(self):
        if (self.parent.isFileLoaded):
            self.tagRoi('NOROOT')
            self.stepToNextRoi()

    def tagBlack(self):
        if (self.parent.isFileLoaded):
            self.tagRoi('BLACK')
            self.stepToNextRoi()

    def tagFungi(self):
        if (self.parent.isFileLoaded):
            self.tagRoi('FUNGI')
            self.stepToNextRoi()

    def goOneTagBack(self):
        if (self.parent.isFileLoaded):
            if self.lstFungiTag:
                lastTag = self.lstFungiTag.pop()
                self.stepOneRoiBack(lastTag)

    def tagRoi(self, tag):
        pos_x, pos_y = self.rois[-1].pos()
        width, height = self.rois[-1].size()
        fungiTag = {'image':self.displayFilename, 'width':width, 'height':height, 'pos_x':pos_x, 'pos_y':pos_y, 'tag':tag}
        self.lstFungiTag.append(fungiTag)

    def stepToNextRoi(self):
        pos_x, pos_y = self.rois[-1].pos()
        width, height = self.rois[-1].size()
        fullImgWidth = self.imgFull.width()
        fullImgHeight = self.imgFull.height()
        if ((pos_x + width) < fullImgWidth): 
            self.rois.append(pg.RectROI([pos_x + width, pos_y], self.rois[-1].size(), centered=False, sideScalers=False, pen=(0,9), movable=False, removable=False))
        else:
            if ((pos_y + height) < fullImgHeight):
                self.rois.append(pg.RectROI([0, pos_y + height], self.rois[-1].size(), centered=False, sideScalers=False, pen=(0,9), movable=False, removable=False))
            else:
                self.rois.append(pg.RectROI([0, 0], self.rois[-1].size(), centered=False, sideScalers=False, pen=(0,9), movable=False, removable=False))
                self.summary()
        for self.roi in self.rois:
            self.fullImageViewBox.addItem(self.roi)
            self.roi.sigRegionChanged.connect(self.updateRoi)
        self.fullImageViewBox.removeItem(self.rois[0])
        del self.rois[0]
        self.updateRoi(self.rois[-1])

    def stepOneRoiBack(self, lastTag):
        displayFilename, width, height, pos_x, pos_y, tag = lastTag.values()
        self.rois.append(pg.RectROI([pos_x, pos_y], (width, height), centered=False, sideScalers=False, pen=(0,9), movable=False, removable=False))
        for self.roi in self.rois:
            self.fullImageViewBox.addItem(self.roi)
            self.roi.sigRegionChanged.connect(self.updateRoi)
        self.fullImageViewBox.removeItem(self.rois[0])
        del self.rois[0]
        self.roiBefore = self.rois[-1]
        self.selected = self.roiBefore.getArrayRegion(self.rootImgData, self.imgFull)
        self.imgZoom.setImage(self.selected, levels=(0, self.rootImgData.max()))
        self.zoomImageViewBox.autoRange()

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Left:
            self.tagFungi()
        elif e.key() == Qt.Key_Right:
            self.tagNoRoot()
        elif e.key() == Qt.Key_Down:
            self.tagNoFungi()
        elif e.key() == Qt.Key_Delete:
            self.goOneTagBack()

    def updateRoi(self, roi):
        self.selected = roi.getArrayRegion(self.rootImgData, self.imgFull)
        self.selectionIsBlack = np.transpose(self.selected.nonzero()).size == 0
        if self.selectionIsBlack:
            self.tagBlack()
        else:
            self.imgZoom.setImage(self.selected, levels=(0, self.rootImgData.max()))
            self.zoomImageViewBox.autoRange()

    def loadImage(self, fileName):
        self.parent.isFileLoaded = True
        self.displayFilename = os.path.splitext(os.path.basename(fileName))[0]
        self.TagFilename = self.displayFilename + '_tagged.csv'
        self.imageNameLabel = self.fullImageWindow.addLabel(self.displayFilename, row=0, col=0, colspan=1, bold=True, size='12pt')
        self.rootImage = pil.Image.open(fileName)
        self.rootImgData = np.array(self.rootImage)
        self.imgFull = pg.ImageItem(self.rootImgData)
        self.fullImageViewBox.addItem(self.imgFull)
        self.fullImageViewBox.autoRange()

    def summary(self):
        lables = ['image', 'width', 'height', 'pos_x', 'pos_y', 'tag']
        dataframe = pandas.DataFrame(self.lstFungiTag, columns=lables)
        tagSummary = dataframe.groupby('tag').size()
        numberNoRoot = 0
        numberBlack = 0
        numberNoFungi = 0
        numberFungi = 0
        for key,value in tagSummary.iteritems():
            if key == 'NOROOT':
                numberNoRoot = value
            elif key == 'ROOT':
                numberNoFungi = value
            elif key == 'BLACK':
                numberBlack = value
            elif key == 'FUNGI':
                numberFungi = value
        self.parent.showSummary(self.displayFilename, numberNoRoot, numberBlack, numberNoFungi, numberFungi)

    def isTagFileAvailable(self):
        if not (self.displayFilename is None):
            if os.path.isfile(self.TagFilename):
                dataframe = pandas.read_csv(self.TagFilename) 
                dataframe.dropna(inplace = True)
                self.lstFungiTag = dataframe[['image', 'width', 'height', 'pos_x', 'pos_y', 'tag']].to_dict(orient='records')
                if self.lstFungiTag:
                    return True
                else:
                    return False
        return False

    def setFirstRoi(self, i):
        self.rois = []
        if self.getLastRoiFromSavedTags():
            pos_x, pos_y, scaleSize = self.getLastRoiFromSavedTags()
            startPosX = pos_x
            startPosY = pos_y
            self.scaleSize = scaleSize
            self.rois.append(pg.RectROI([startPosX, startPosY], [self.scaleSize, self.scaleSize], centered=False, sideScalers=False, pen=(0,9), movable=False, removable=False))
            self.stepToNextRoi()
        else:
            startPosX = 0
            startPosY = 0
            self.scaleSize = i
            self.rois.append(pg.RectROI([startPosX, startPosY], [self.scaleSize, self.scaleSize], centered=False, sideScalers=False, pen=(0,9), movable=False, removable=False))
        for self.roi in self.rois:
            self.roi.sigRegionChanged.connect(self.updateRoi)
            self.fullImageViewBox.addItem(self.roi)
        self.updateRoi(self.rois[-1])

    def reset(self):
        self.rois = []

    def getLastRoiFromSavedTags(self):
        if os.path.isfile(self.TagFilename):
            pos_x = 0
            pos_y = 0
            scaleSize = self.scaleSize
            dataframe = pandas.read_csv(self.TagFilename) 
            dataframe.dropna(inplace = True)
            self.lstFungiTag = dataframe[['image', 'width', 'height', 'pos_x', 'pos_y', 'tag']].to_dict(orient='records')
            if self.lstFungiTag:
                lastPos_x = (self.lstFungiTag[-1])['pos_x']
                lastPos_y = (self.lstFungiTag[-1])['pos_y']
                fromFileRoiWidth = dataframe.width.unique()[0]
                fromFileRoiHeight = dataframe.height.unique()[0]
                numberOfDifferentRoiWidth = len(dataframe.width.unique())
                numberOfDifferentRoiHeight = len(dataframe.height.unique())
                if (numberOfDifferentRoiWidth>=2) or (numberOfDifferentRoiHeight>=2):
                    print("Error: Different sizes in one file")
                elif fromFileRoiWidth != fromFileRoiHeight:
                    print("Error: Different sizes for a tag")
                return [lastPos_x, lastPos_y, fromFileRoiWidth]
            else:
                return False
        else:
            return False

    def saveResults(self):
        if self.lstFungiTag:
            lables = ['image', 'width', 'height', 'pos_x', 'pos_y', 'tag']
            dataframe = pandas.DataFrame(self.lstFungiTag, columns=lables)
            export_csv = dataframe.to_csv(self.TagFilename, index = None, header=True)
        else:
            lables = ['image', 'width', 'height', 'pos_x', 'pos_y', 'tag']
            dataframe = pandas.DataFrame([], columns=lables)
            export_csv = dataframe.to_csv(self.TagFilename, index = None, header=True)

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    w = FungiTaggerMainWindow()
    w.showMaximized()
    sys.exit(app.exec_())
